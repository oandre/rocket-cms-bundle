<?php

namespace Rocket\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Rocket\CmsBundle\Content\BlankContent;

/**
 * This controller can't use routing annotations!
 * It is configured in routing.yml to match the route after passing by all frontend routes.
 *
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 */
class CmsController extends Controller
{
    public function indexAction($uri)
    {
        $cms = $this->get('rocket.cms');
        $locale = $this->getRequest()->getLocale();
        /** @var \Rocket\CmsBundle\Content\AbstractContent $page */
        $page = $cms->findByUri($uri, $locale);

        if ($page instanceof BlankContent) {
            throw $this->createNotFoundException(sprintf('CMS with uri [%s] and locale [%s] not found.', $uri, $locale));
        }

        $template =  $this->container->getParameter('cms.frontend_template');

        $responseText = $page->render(array(
            'cms_frontend_template' => $template,
        ));

        return new Response($responseText);
    }
}
